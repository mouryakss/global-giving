import React from 'react';
import Axios from 'axios';

function PostForm(props){
    return (
    <div className="contact-us">
        <form action="#">
            <label htmlFor="name">What is your Organization's Name ?</label>
            <input type="text" id="inputKey" placeholder="Organization Name"/>
            <label htmlFor="org_addressing">What Problems/Challenges Organization is addressing ?</label>
            <input type="text" id="org_addressing" placeholder="Problem Addressing"/>
            <label htmlFor="org_strength">What are your Organization's Strengths/Expertise ?</label>
            <input type="text" id="org_strength" placeholder="Organization Strength"/>
            <label htmlFor="org_programs">What are your Organization's Programs ?</label>
            <input type="text" id="org_programs" placeholder="Organization Programs"/>
            <label htmlFor="org_director">How is your Organization's Director ?</label>
            <input type="text" id="org_director_1" placeholder="Organization Director Name"/>
            <br></br> 
            <input type="text" id="org_director_2" placeholder="Organization Director Title"/>

            <label htmlFor="org_main_address">What is your organizations Main Address ?</label>    
            <input type="text" id="org_main_address_street1" placeholder="Street 1"/>
            <br></br> 
            <input type="text" id="org_main_address_street2" placeholder="Street 2"/>
            <br></br>
            <input type="text" id="org_main_address_city" placeholder="City"/>
            <br></br>
            <input type="text" id="org_main_address_state" placeholder="State"/>
            <br></br>
            <input type="text" id="org_main_address_postal" placeholder="Postal"/>
            <br></br>

            <label htmlFor="org_country_address">What is your organizations Country Address ?</label>    
            <input type="text" id="org_country_address" placeholder="Address"/>
                
            <label htmlFor="org_phone_number">What is your organizations Phone Number ?</label>    
            <input type="text" id="inputValue" placeholder="Phone Number"/>

            <label htmlFor="org_url">What is your organizations URL ?</label>    
            <input type="text" id="org_url" placeholder="URL"/>
        
            <label htmlFor="org_scope">What is your organizations scope ?</label>    
            <input type="text" id="org_scope" placeholder="Scope"/>

            <label htmlFor="org_staff_number">What is your organizations Staff Number ?</label>    
            <input type="number" id="org_employee" placeholder="Employee Number"/>
            <br></br>
            <input type="number" id="org_volunteers" placeholder="Volunteers"/>

            <label htmlFor="org_year_founded">When was your organization Founded ?</label>    
            <input type="date" id="org_year_founded" placeholder="Date"/>
            <label htmlFor="public Info">I agree to make this organization's financial information Public ?</label>
            <select id="public Info">
              <option value="public_info_yes">Yes</option>
              <option value="public_info_no">No</option>
              </select>
            <label htmlFor="current_year_operation_budget">What is your Organization's Current Year Operating Budget ?</label>
            <input type="text" id="current_year_operation_budget" placeholder="Operating Budget"/>
            
            <label htmlFor="previous_year_operation_budget">What is your Organization's previous_year_operation_budget ?</label>
            <input type="text" id="previous_year_operation_budget" placeholder="Operating Budget"/>
            
            <label htmlFor="maximum_operation_budget">What is your Organization's Maximum Operating Budget ?</label>
            <input type="text" id="maximum_operation_budget" placeholder="Operating Budget"/>
            
            <label htmlFor="previous_year_overhead">What is your Organization's Previous Year Overhead ? </label>
            <input type="text" id="previous_year_operation_budget" placeholder="Percentage"/>
            
            <label htmlFor="religious_affiliation_no">What is your Organization's Religious Affiliation No ? </label>
            <input type="text" id="religious_affiliation_no" placeholder="Religios Affiliation  No"/>
            
            <label htmlFor="organization_other_funding_source_no">What is your Organization's Other Funding Sources No ? </label>
            <input type="text" id="organization_other_funding_source_no" placeholder="Other Funding Sources No"/>
            <label htmlFor="organization_administrative_information">What is your Organization's Board of Directors ?</label>
            <input type="text" id="organization_administrative_information" placeholder="Board of Directors"/>
            
            <label htmlFor="organization_executive_managers">What is your Organization's Executive Managers ?</label>
            <input type="text" id="organization_executive_managers" placeholder="Executive Managers"/>
            
            <label htmlFor="organization_financial_institutions_bank">What is your Organization's Financial Institutions/Banks ?</label>
            <input type="text" id="organization_financial_institutions_bank" placeholder="Institutions/Banks"/>
            
            <label htmlFor="organization_executive_managers">What is your Organization's Executive Managers ?</label>
            <input type="text" id="organization_executive_managers" placeholder="Executive Managers"/>
            
            <label htmlFor="project_title">What is your Project Title ?</label>
            <textarea id="project_title" cols="30" rows="10" placeholder="Title"></textarea>
            
            <label htmlFor="project_summary">What is your Project Summary ?</label>
            <textarea id="project_summary" cols="30" rows="10" placeholder="Summary"></textarea>  
            
            <label htmlFor="project_need_and_beneficiaries">What are your Project Need and Beneficiaries ?</label>
            <textarea id="project_need_and_beneficiaries" cols="30" rows="10" placeholder="Needs and Beneficiaries"></textarea>
            
            <label htmlFor="project_activities">What are your Project Activities ?</label>
            <textarea id="project_activities" cols="30" rows="10" placeholder="Activities"></textarea>
            
            <label htmlFor="donation_options">What are your Donation Options ?</label>
            <textarea id="donation_options" cols="30" rows="10" placeholder="Please Fill-out At least 3"></textarea>
            
            <label htmlFor="potential_long_term_impact">What are your Potential Long-Term Impact ?</label>
            <textarea id="potential_long_term_impact" cols="30" rows="10" placeholder="Please Mention it Briefly"></textarea>
            
            <label htmlFor="project_message">What are your Project Message ?</label>
            <textarea id="project_message" cols="30" rows="10" placeholder="Messge"></textarea>
            
            <label htmlFor="person_qoute_name">What is the Name of the Person Qouted ?</label>
            <input type="text" id="person_qoute_name" placeholder="Qouted Name"/>
            
            <label htmlFor="description_of_person_quoted">Please provide description of person Qouted ?</label>
            <textarea id="description_of_person_quoted" cols="30" rows="10" placeholder="Description"></textarea>
            
            <label htmlFor="project_country">Where is the Project Country Located ?</label>
            <input type="text" id="project_country" placeholder="Country Name"/>
            
            <label htmlFor="project_theme">Where is the Project Theme ?</label>
            <input type="text" id="project_theme" placeholder="Theme"/>
            
            <label htmlFor="project_activity_type">Please select the Project Activity Type ?</label>
            <select id="project_activity_type">
                <option value="project_activity_type_patronage">Patronage</option>
                <option value="project_activity_type_charity">Charity</option>
                <option value="project_activity_type_development">Development</option>
            </select>
            
            <label htmlFor="project_funding_requested_in_dollars">Please provide your Project Funding Requested in Dollar ?</label>
            <textarea id="project_funding_requested_in_dollars" cols="30" rows="10" placeholder="Please Describe"></textarea>
            <br></br>
            <input type="text" id="project_funding_requested_in_dollars" placeholder="In Dollars"/>
            
            
            <label htmlFor="keywords">Please provide any keywords ?</label>
            <textarea id="keywords" cols="30" rows="10" placeholder="Please Describe"></textarea>
<br></br>    <br></br>      <br></br>      
        <a className="buttonNext" href="http://localhost:5000/eg001" id="buttonAdd">Next Page <i className="fa fa-arrow-right"></i></a>
        </form>
    </div>
    )
}

export default PostForm;