import logo from './logo.svg';
import './App.css';
import PostForm from './components/PostForm';

function App() {
  return (
    <div className="App">
    <div className="headercontent">      
      <img className="logoimg" src="https://www.globalgiving.org/img/logos/gg_horizontal_color_600.png" alt="Global Giving" height="32px" width="220px" align="left"></img>
      <h3 className="learnmore" align="right"><a className="buttonNext" href="https://www.globalgiving.org/nonprofits/">Learn More</a></h3>
    </div>
      <br>
      </br>
      <h3 className="titlenpf">Nonprofit Partner Registration</h3>

     <PostForm />     
      <div className="card">
        <img className="logoimg" src="https://www.globalgiving.org/img/logos/gg_horizontal_color_600.png" alt="Global Giving" height="32px" width="220px" align="center"></img>
        <br></br><br></br>
        <div>
          <a href="mailto:help@globalgiving.org"><i className="fa fa-envelope"></i></a> 
          <a href="https://twitter.com/globalgiving"><i className="fa fa-twitter"></i></a>  
          <a href="https://www.instagram.com/globalgiving/"><i className="fa fa-instagram"></i></a>  
          <a href="https://www.linkedin.com/company/globalgiving/"><i className="fa fa-linkedin"></i></a>  
          <a href="https://www.facebook.com/GlobalGiving/"><i className="fa fa-facebook"></i></a> 
        </div>
      </div>
      <br></br>      <br></br>      
    </div>
  );
}

export default App;
